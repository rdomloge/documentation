#!/bin/bash

sudo yum -y update
sudo yum -y upgrade

sudo yum -y remove java-1.7.0-openjdk.x86_64
sudo yum -y install java-1.8.0-openjdk-devel
sudo yum -y install htop
sudo yum -y install git

# get and install Jenkins 
sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins.io/redhat-stable/jenkins.repo

sudo rpm --import http://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum install -y jenkins
sudo service jenkins start # start|stop|restart available
sudo chkconfig jenkins on # start Jenkins at boot time

wget http://apache.mirrors.nublue.co.uk/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz

sudo tar xvzf apache-maven-3.6.0-bin.tar.gz -C /usr/local

sudo ln -s /usr/local/apache-maven-3.6.0 /usr/local/maven
sudo ln -s /usr/local/maven/bin/mvn /bin/mvn
