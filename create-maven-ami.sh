#!/bin/bash

sudo yum -y update 
sudo yum -y upgrade

sudo yum -y remove java-1.7.0-openjdk.x86_64
sudo yum -y install java-1.8.0-openjdk-devel
sudo yum -y install htop
sudo yum -y install git

wget http://apache.mirrors.nublue.co.uk/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz

sudo tar xvzf apache-maven-3.6.0-bin.tar.gz -C /usr/local

sudo ln -s /usr/local/apache-maven-3.6.0 /usr/local/maven
sudo ln -s /usr/local/maven/bin/mvn /bin/mvn
echo Creating maven project
mvn archetype:generate -DgroupId=com.qa -DartifactId=hello-maven -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
echo Moving project to user home
sudo cp -R /hello-maven /home/ec2-user
sudo chown -R ec2-user:ec2-user /home/ec2-user/hello-maven
sudo rm -r /hello-maven

# Housekeeping
sudo rm /apache-maven-3.6.0-bin.tar.gz
sudo rm /create-maven-ami.sh
